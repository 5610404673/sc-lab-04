package lab04;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI extends JFrame {
	JFrame frame = new JFrame();
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	JTextField textfield = new JTextField(10);
	JButton button = new JButton("Run");
	JTextArea text = new JTextArea();

	String choice[] = { " ", "star1", "star2", "star3", "star4", "star5" };
	JComboBox box = new JComboBox(choice);

	public GUI() {
		Build();
	}

	Controller control = new Controller();

	public void Build() {
		setLayout(new BorderLayout());
		setLayout(new GridLayout(1, 2));
		add(panel1);
		add(panel2);

		// panel1
		panel1.setLayout(new BorderLayout());
		panel1.add(text);

		// panel2
		setLayout(new GridLayout(1, 3));
		panel2.add(textfield);
		panel2.add(box);
		panel2.add(button);

		// button
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (box.getSelectedItem() == "star1") {
					text.setText(control.build1(Integer.parseInt(textfield
							.getText())));

				}
				if (box.getSelectedItem() == "star2") {
					text.setText(control.build2(Integer.parseInt(textfield
							.getText())));
				}
				if (box.getSelectedItem() == "star3") {
					text.setText(control.build3(Integer.parseInt(textfield
							.getText())));
				}
				if (box.getSelectedItem() == "star4") {
					text.setText(control.build4(Integer.parseInt(textfield
							.getText())));
				}
				if (box.getSelectedItem() == "star5") {
					text.setText(control.build5(Integer.parseInt(textfield
							.getText())));

				}
			}

		});

	}

}
