package lab04;

public class Controller {

	Nested_loop nest = new Nested_loop();

	public String build1(int num) {
		String a1 = nest.star1(num);
		return a1;
	}

	public String build2(int num) {
		String a2 = nest.star2(num);
		return a2;
	}

	public String build3(int num) {
		String a3 = nest.star3(num);
		return a3;
	}

	public String build4(int num) {
		String a4 = nest.star4(num);
		return a4;
	}

	public String build5(int num) {
		String a5 = nest.star5(num);
		return a5;
	}

}
